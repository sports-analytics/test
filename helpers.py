import numpy as np
import pandas as pd
import json


class Parser:
    """
        Parser class to read Betfair log files
        Horse Racing:
            the log file contain information about 2 different markets:
                1- The win market - '2m Mdn Hrd' - marketId: '1.170226122'
                2 - the place market with - 'To Be Placed' - marketId: '1.170226123'
    """

    def __init__(self, log_file="assessment_log.json"):
        self.log_file = log_file
        self.market_names = ['TBP', 'Win']
        self.mapping = {'1.170226122': '1', '1.170226123': '2'}

    def __read_json(self):
        """
        Reads the json file and returns the data it contains

        :return:
        """
        with open(self.log_file) as json_file:
            data = json_file.readlines()
        return data

    def __process_order(self, order):
        """
            Process single order

        :param order:
        :return:
        """
        status = order['status']
        sizeMatched = order['sizeMatched']
        orderStatus = order['orderStatus']
        placedDate = order['placedDate']
        avgPriceMatched = order['averagePriceMatched']
        orderType = order['instruction']['orderType']
        selectionId = order['instruction']['selectionId']
        side = order['instruction']['side']
        price = order['instruction']['limitOrder']['price']
        try:
            persistenceType = order['instruction']['limitOrder']['persistenceType']
        except Exception as Error:
            print(Error)
            persistenceType = order['instruction']['limitOrder']['timeInForce']

        betId = order['betId']
        size = order['instruction']['limitOrder']['size']
        tmp = pd.DataFrame({'averagePriceMatched': [avgPriceMatched],
                            'betId': [betId],
                            'persistenceType': [persistenceType],
                            'price': [price],
                            'size': [size],
                            'orderType': [orderType],
                            'selectionId': [selectionId],
                            'side': [side],
                            'orderStatus': [orderStatus],
                            'placedDate': [placedDate],
                            'sizeMatched': [sizeMatched],
                            'status': [status]})
        return tmp

    def __process_cancellation(self, cancel):
        """

            Process cancellation

        :param cancel:
        :return:
        """
        status = cancel['status']
        sizeMatched = None
        orderStatus = None
        placedDate = cancel['cancelledDate']
        avgPriceMatched = None
        orderType = 'CANCEL'
        selectionId = None
        side = None
        price = None
        persistenceType = None
        betId = cancel['instruction']['betId']
        size = cancel['sizeCancelled']
        tmp = pd.DataFrame({'averagePriceMatched': [avgPriceMatched],
                            'betId': [betId],
                            'persistenceType': [persistenceType],
                            'price': [price],
                            'size': [size],
                            'orderType': [orderType],
                            'selectionId': [selectionId],
                            'side': [side],
                            'orderStatus': [orderStatus],
                            'placedDate': [placedDate],
                            'sizeMatched': [sizeMatched],
                            'status': [status]})
        return tmp

    def orders_market(self, market_id):
        data = self.__read_json()
        # Lines 0 and 1 are the description of the 2 races present in this log file
        results = pd.DataFrame({'averagePriceMatched': [],
                                'betId': [],
                                'persistenceType': [],
                                'price': [],
                                'size': [],
                                'orderType': [],
                                'selectionId': [],
                                'side': [],
                                'orderStatus': [],
                                'placedDate': [],
                                'sizeMatched': [],
                                'status': []})
        for i in data:
            if 'marketId' in json.loads(i)['app_data'].keys():
                if json.loads(i)['app_data']['marketId'] == market_id:
                    if 'instructionReports' in json.loads(i)['app_data'].keys():
                        print(json.loads(i)['app_data'])
                        list_orders = json.loads(i)['app_data']['instructionReports']
                        for order in list_orders:
                            print(order)
                            if 'errorCode' not in order.keys():
                                tmp = self.__process_order(order)
                            else:
                                tmp = self.__process_cancellation(order)
                            results = pd.concat([results, tmp])
        results.reset_index(inplace=True, drop=True)
        return results

    def order_book(self, market_id):
        """

        TODO: Regex to reduce the search space to the lines that corresponds to the market_id
        :param market_id:
        :return:
        """
        n = 0
        data = self.__read_json()
        # Lines 0 and 1 are the description of the 2 races present in this log file
        results = pd.DataFrame({'time': [],
                                'selectionId': [],
                                'bprice1': [],
                                'bsize1': [],
                                'bprice2': [],
                                'bsize2': [],
                                'bprice3': [],
                                'bsize3': []})
        for i in data:
            n += 1
            if 'marketId' in json.loads(i)['app_data'].keys():
                if json.loads(i)['app_data']['marketId'] == market_id:
                    if 'status' in json.loads(i)['app_data'].keys():
                        if 'runners' in json.loads(i)['app_data'].keys():
                            time = json.loads(i)['app_data']['lastMatchTime']
                            list_runners = json.loads(i)['app_data']['runners']
                            if n % 100 == 0: print(n)
                            for runner in list_runners:
                                if runner['status'] != 'ACTIVE':
                                    continue
                                selectionId = runner['selectionId']

                                bprice1 = runner['ex']['availableToBack'][0]['price']
                                bsize1 = runner['ex']['availableToBack'][0]['size']
                                bprice2 = runner['ex']['availableToBack'][1]['price']
                                bsize2 = runner['ex']['availableToBack'][1]['size']
                                bprice3 = runner['ex']['availableToBack'][2]['price']
                                bsize3 = runner['ex']['availableToBack'][2]['size']

                                try:
                                    lprice1 = runner['ex']['availableToLay'][0]['price']
                                    lsize1 = runner['ex']['availableToLay'][0]['size']
                                    lprice2 = runner['ex']['availableToLay'][1]['price']
                                    lsize2 = runner['ex']['availableToLay'][1]['size']
                                    lprice3 = runner['ex']['availableToLay'][2]['price']
                                    lsize3 = runner['ex']['availableToLay'][2]['size']
                                except IndexError:
                                    lprice1 = None
                                    lsize1 = None
                                    lprice2 = None
                                    lsize2 = None
                                    lprice3 = None
                                    lsize3 = None

                                tmp = pd.DataFrame({'time': [time],
                                                    'selectionId': [selectionId],
                                                    'bprice1': [bprice1],
                                                    'bsize1': [bsize1],
                                                    'bprice2': [bprice2],
                                                    'bsize2': [bsize2],
                                                    'bprice3': [bprice3],
                                                    'bsize3': [bsize3],
                                                    'lprice1': [lprice1],
                                                    'lsize1': [lsize1],
                                                    'lprice2': [lprice2],
                                                    'lsize2': [lsize2],
                                                    'lprice3': [lprice3],
                                                    'lsize3': [lsize3]
                                                    })
                                results = pd.concat([results, tmp])
        results.reset_index(inplace=True, drop=True)
        return results

    def compute_pnl_selection(self, selectionId, market='Win'):
        if market not in self.market_names:
            raise Exception('market parameter must be either: Win or TBP')
        market = '1' if market == 'Win' else '2'
        orders = pd.read_csv('orders_' + market + '.csv')
        orders = orders[orders['sizeMatched'] > 0]
        orders = orders[orders['selectionId'] == selectionId].reset_index(drop=True)
        obook = pd.read_csv('order_book_' + market + '.csv')
        final_price = float(obook[obook['selectionId'] == selectionId].tail(1)['bprice1'])
        n = orders.shape[0]
        pnl_win = 0.0
        pnl_loss = 0.0
        pnl = 0.0
        side = ''
        total_size = 0.0
        average_price = 0.0
        for i in range(n):
            average_price += orders.loc[i, 'sizeMatched'] * orders.loc[i, 'price']
            total_size += orders.loc[i, 'sizeMatched']
            if orders.loc[i, 'side'] == 'BACK':
                side = 'BACK'
                pnl_win += orders.loc[i, 'sizeMatched'] * (orders.loc[i, 'price'] - 1)
                pnl_loss += -orders.loc[i, 'sizeMatched']

            else:
                pnl_win += orders.loc[i, 'sizeMatched']
                pnl_loss += -orders.loc[i, 'sizeMatched'] * (orders.loc[i, 'price'] - 1.0)
                side = 'LAY'
        average_price /= total_size
        print(side, pnl_win, pnl_loss, total_size, average_price, final_price, selectionId)
        return side, pnl_win, pnl_loss, total_size, average_price, final_price, selectionId

    def compute_total_pnl(self, market='Win'):
        if market not in self.market_names:
            raise Exception('market parameter must be either: Win or TBP')
        suffix = '1' if market == 'Win' else '2'
        orders = pd.read_csv('orders_' + suffix + '.csv')
        orders = orders[orders['sizeMatched'] > 0].reset_index(drop=True)
        selectionIds = np.unique(orders['selectionId'].values)
        pnl = []
        for selectionId in selectionIds:
            pnl += self.compute_pnl_selection(selectionId=selectionId, market=market)

        return pnl
